from gspread.exceptions import APIError
from selenium.webdriver import Chrome
from selenium.webdriver.support.ui import WebDriverWait as Wait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
from time import sleep, time
from gspread_formatting import CellFormat, Color, format_cell_ranges
import gspread


HOST = "https://kazanexpress.ru/"
MAX_ROWS = 5000
MY_STORE = "hornY rabbiT"
ROW = [[
    "Ссылка",
    "Магазин (название)",
    "Наименование товара",
    "Цена",
    "Рейтинг",
    "Количество заказов",
    "Количество товара",
    "Место выдачи (позиция)"
]]
FMT = CellFormat(
    backgroundColor=Color(201/255, 218/255, 248/255),
    horizontalAlignment='CENTER'
)
FMT_SHOP = CellFormat(
    backgroundColor=Color(217/255, 234/255, 211/255),
    horizontalAlignment='LEFT'
)


def main():
    start = time()

    some_goods = []
    print("Ведите название или названия товаров через запятую \",\"")
    for el in input(" -- ").split(","):
        if el and len(el) > 1:
            some_goods.append(el.strip())

    browser = Chrome(".\\chromedriver.exe")
    sa = gspread.service_account()
    sheet = sa.open("Try google API")

    for element in some_goods:
        try:
            curr_row = 2
            sheet.add_worksheet(element.lower(), MAX_ROWS, 10)
            curr_sheet = sheet.worksheet(element.lower())
        except APIError:
            curr_sheet = sheet.worksheet(element.lower())
            curr_sheet.clear()
        curr_sheet.update("A1:H1", ROW)
        format_cell_ranges(curr_sheet, [("A1:H1", FMT)])
        url = f'https://kazanexpress.ru/search?query={element.replace(" ", "%20")}'
        first_part(browser, url, curr_sheet, curr_row=curr_row)

    browser.close()
    print("time: ", time() - start)


def first_part(browser, url, curr_sheet, curr_row: int = 2):
    """  1_|__ пример: https://kazanexpress.ru/search?query=шампунь%20sfafg """
    links = _get_first_links_from_catalog(browser, url)
    for link in links:
        # if curr_row < row_number:
        #     curr_row += 1
        #     continue
        data, store_name = _get_info(browser, link)
        if store_name == MY_STORE:
            format_cell_ranges(curr_sheet, [(f"A{curr_row}:H{curr_row}", FMT_SHOP)])

        data.append(curr_row - 1)
        data = [data]
        curr_sheet.update(f"A{curr_row}:H{curr_row}", data)
        print("Row  --  ", curr_row, "\nData  --  ", data)
        curr_row += 1


def _get_first_links_from_catalog(browser, url):
    browser.get(url)
    list_of_items = []
    i = 1
    sleep(10)

    while True:
        xpath = f"//*[@id=\"category-products\"]/div[{i}]/a"
        element = get_element_by_xpath(browser=browser, xpath=xpath)
        i += 1
        try:
            link = element.get_attribute("href")
        except Exception as e:
            print(e)
            break

        list_of_items.append(link)
        if i >= 49:  # на одной странице находится ровно 48 товаров
            element = get_elements_by_class_name(browser=browser, class_name="pagination-navigation")[-1]
            if element.is_enabled():
                element.click()
                i = 1
            else:
                break
    return list_of_items


def _get_info(browser, url):
    """ 2_|__ пример: https://kazanexpress.ru/product/Smartfon-Xiaomi-Redmi-Note-10S-664GB-1767788"""
    browser.get(url)
    id = "product-info"
    rate, prod_name, store_name, amount, currency, number_of_sales = "-", "-", "-", "-", "-", "-"
    try:
        product_block = get_element_by_id(browser, id)
        element = product_block.find_elements(by=By.TAG_NAME, value="a").pop()

        rate = product_block.find_elements(by=By.CLASS_NAME, value="rating").pop().text.replace("\n", " ")
        prod_name = product_block.find_elements(by=By.CLASS_NAME, value="title").pop().text
        store_name = element.text
        amount = product_block.find_elements(by=By.CLASS_NAME, value="available-amount").pop().text
        currency = product_block.find_elements(by=By.CLASS_NAME, value="currency").pop().text.replace(" ", "")
        number_of_sales = product_block.find_elements(by=By.CLASS_NAME, value="offer-text").pop().text
        # store_link = element.get_attribute("href")
        # delivery = product_block.find_elements(by=By.CLASS_NAME, value="product-info-item-value").pop().text
    except Exception as e:
        print(e)


    return [url, store_name, prod_name, currency, rate, number_of_sales, amount], store_name


def get_element_by_xpath(browser, xpath: str, delay: int = 3):
    some_element = None
    try:
        some_element = Wait(browser, delay).until(ec.presence_of_element_located((By.XPATH, xpath)))
    except TimeoutException:
        pass
    return some_element


def get_element_by_class_name(browser, class_name: str, delay: int = 3):
    some_element = None
    try:
        some_element = Wait(browser, delay).until(ec.presence_of_element_located((By.CLASS_NAME, class_name)))
    except TimeoutException:
        pass
    return some_element


def get_elements_by_class_name(browser, class_name: str, delay: int = 2):
    some_elements = None
    try:
        Wait(browser, delay).until(ec.presence_of_element_located((By.CLASS_NAME, class_name)))
        some_elements = browser.find_elements(by=By.CLASS_NAME, value=class_name)
    except TimeoutException:
        pass
    return some_elements


def get_element_by_id(browser, id: str, delay: int = 2):
    some_element = None
    try:
        some_element = Wait(browser, delay).until(ec.presence_of_element_located((By.ID, id)))
    except TimeoutException:
        pass
    return some_element


if __name__ == "__main__":
    main()
